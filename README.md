# Material UI Components

[The world's most popular React UI framework - Material-UI](https://material-ui.com/) のコンポーネントのまとめ

## Layout

### Grid

- Limitations
  - Negative margin
    - **Not using the spacing feature** and implementing it in user space spacing={0} (default).
    - **Applying padding to the parent** with at least half the spacing value applied to the child
    - Adding overflow-x: hidden; to the parent.
  - white-space: nowrap;

## Component Demos

### App Bar

The top App Bar provides content and actions related to the current screen. It’s used for branding, screen titles, navigation, and actions.

It can transform into a contextual action bar or used as a navbar.

### Autocomplete

Material-UI doesn't provide any high-level API for solving this problem. We encourage people relying on the solutions the React community has built.

### Avatars

### Badge

Badge generates a small badge to the top-right of its child(ren).

### Bottom Navigation

Bottom navigation bars display three to five destinations at the bottom of a screen. Each destination is represented by an icon and an optional text label. When a bottom navigation icon is tapped, the user is taken to the **top-level navigation** destination associated with that icon.

When there are only three actions, display both icons and text labels at all times. If there are four or five actions, display inactive views as icons only.

### Buttons

- Contained Buttons: They contain actions that are **primary to your app**.
- Outlined Buttons: They contain actions that are **important, but aren’t the primary action in an app**.
- Text Buttons
- Floating Action Buttons: Only use a FAB if it is the most suitable way to present a **screen’s primary action**.
- Complex Buttons: The Text Buttons, Contained Buttons, Floating Action Buttons and Icon Buttons are **built on top of the same component: the ButtonBase**. You can take advantage of this lower level component to build custom interactions.
- Third-party routing library: The ButtonBase component provides **a property to handle this use case**: **component**.

### Cards

Cards are surfaces that display content and actions on a single topic.

They should be easy to scan for relevant and actionable information. Elements, like text and images, should be placed on them in a way that clearly indicates hierarchy.

- Simple Card: Although cards can support multiple actions, UI controls, and an overflow menu, use restraint and **remember that cards are entry points** to more complex and detailed information.

### Chips

Chips allow users to enter information, make selections, filter content, or trigger actions.

While included here as a standalone component, **the most common use will be in some form of input**, so some of the behaviour demonstrated here is not shown in context.

### Dialogs

### Dividers

The divider renders as a `<hr>` by default. You can save rendering this DOM element by using the divider property on the ListItem component.

- List>ListItem+Divider+ListItem+Divider+ListItem

HTML5 Specification: We need to make sure the Divider is rendered as a li to match the HTML5 specification. The examples below show two ways of achieving this.

- `<li><Divider /></li>`
- `<Divider variant="inset" />`

### Drawer

Navigation drawers provide access to **destinations and app functionality**, such as switching accounts.

Clipped under the app bar: Apps focused on productivity that require balance across the screen.

### Expansion Panels

### Grid List

- Single line Grid list: **Horizontally scrolling grid lists are discouraged** because the scrolling interferes with typical reading patterns, affecting comprehension. **One notable exception is** a horizontally-scrolling, single-line grid list of images, such as **a gallery**.

### Lists

[Lists - Material Design](https://material.io/design/components/lists.html)

- Simple List
- Nested List: List>ListItem>Collapse>List
- List Controls
  - primary action ... list items
  - secondary action ... a separate target
  - Checkbox: A checkbox can either be **a primary action or a secondary action**.
  - Switch: The switch is the **secondary action** and a separate target.
  - Pinned Subheader List: This feature is relying on the **CSS sticky positioning**.
- API
  - `<Collapse />`
  - `<Divider />`
  - `<List />`
  - `<ListItem />`
  - `<ListItemAvatar />`
  - `<ListItemIcon />`
  - `<ListItemSecondaryAction />`
  - `<ListItemText />`
  - `<ListSubheader />`

### Menus

- Limitations: There is a **flexbox bug** that prevents text-overflow: ellipse from working in a flexbox layout. You can use the **Typography component** to workaround this issue

### Paper

### Pickers

Native input controls support by browsers isn't perfect.

- Date picker
- Date & Time picker
- Time picker

### Progress

- Determinate: Indicators display how long an operation will take.
- Indeterminate: Indicators visualize an unspecified wait time.

### Selection Controls

[Selection controls - Material Design](https://material.io/design/components/selection-controls.html)

- Radio Buttons
  - Radio buttons allow the user to **select one option** from a set. Use radio buttons **when the user needs to see all available options**. If available options can be collapsed, consider using a dropdown menu because it uses less space.
- Checkboxes
  - Checkboxes allow the user to select **one or more items** from a set. **Checkboxes can be used to turn an option on or off**.
  - If you have multiple options appearing in a list, you can preserve space by using checkboxes instead of on/off switches. If you have a single option, avoid using a checkbox and use an on/off switch instead.
    - [Material Design When to use checkboxes instead of toggles](https://material.io/design/components/selection-controls.html?image=https%3A%2F%2Fstorage.googleapis.com%2Fspec-host-backup%2Fmio-design%252Fassets%252F16aHMSV5UVzxn_q94taFH9PpY4edwZQML%252Fselectioncontrols-usage-whentouse-do-checkboxes.png)
- Switches

### Selects

Native Select: As the user experience can be improved on mobile **using the native select of the platform**, we allow such pattern.

### Snackbars

Snackbars contain **a single line of text** directly related to the operation performed. They may contain a text action, but **no icons**. You can use them to display **notifications**.

**Only one snackbar** may be displayed at a time.

Don't block the floating action button

### Steppers

### Tables

Data tables can include:

- A corresponding visualization
- Navigation
- Tools to query and manipulate data

### Tabs

- Fixed Tabs
  - The **variant="fullWidth"** property should be used **for smaller views**.
  - The **centered property** should be used **for larger views**.
- Scrollable Tabs
  - Automatic Scroll Buttons
  - Forced Scroll Buttons
  - Prevent Scroll Buttons

### Text Fields

The TextField wrapper component is a complete form control **including a label, input and help text**.

- Customized inputs
  - **Customization does not stop at CSS**, you can use composition to build custom components and give your app a unique feel. Below is an example using the InputBase component, inspired by Google Maps.
- Input Adornments
  - Input allows the provision of InputAdornment. These can be used to **add a prefix, a suffix or an action to an input**.
- Layout
  - TextField, FormControl allow the specification of **margin to alter the vertical spacing** of inputs. Using **none (default)** will not apply margins to the FormControl, whereas dense and normal will as well as alter other styles to meet the specification.
- **Limitations**
  - **The input label "shrink" state isn't always correct**.
  - In some circumstances, we can't determine the "shrink" state (**number input, datetime input, Stripe input**). You might notice an overlap.
  - To workaround the issue, you can force the "shrink" state of the label.
    - `<TextField InputLabelProps={{ shrink: true }} />`
    - `<InputLabel shrink>Count</InputLabel>`
- Formatted inputs
  - You have to provide a **custom implementation of the <input> element** with the inputComponent property.
- API
  - `<FilledInput />`
  - `<FormControl />`
  - `<FormHelperText />`
  - `<Input />`
  - `<InputAdornment />`
  - `<InputBase />`
  - `<InputLabel />`
  - `<OutlinedInput />`
  - `<TextField />`

### Tooltips
